import express from 'express';
import {App} from "./App";


(async () => {
    const app = new App();
    await app.init();
    app.getExpressApp().listen(process.env.PORT, () => {
        console.log(`Server is up on ${process.env.PORT} ${process.env.ENVIRONMENT}!`);
    })
})();

