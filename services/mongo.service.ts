import {MongoInterface} from "../interfaces/ConfigInterface";
import {MongoClient} from "mongodb";

export class MongoService {

    config: MongoInterface;
    static db: any;
    static collections:any = {};

    constructor(config: MongoInterface) {
        this.config = config;
        const a = MongoService.db;
    }

    init() {
        return new Promise((resolve, reject) => {
            MongoClient.connect(this.config.url, async (err: any, connection: MongoClient | undefined) => {
                if (err || !connection) {
                    return reject("Couldn't connect to Mongo");
                }
                MongoService.db = await connection.db(this.config.db);
                if (this.config.collections && Object.keys(this.config.collections).length) {
                    for (let collection in this.config.collections) {
                        if (this.config.collections.hasOwnProperty(collection)) {
                            MongoService.collections[collection] = await MongoService.db.collection(this.config.collections[collection]);
                        }
                    }
                    resolve(MongoService.collections);
                } else {
                    reject("No collections provided!");
                }
            })
        })
    }

}
