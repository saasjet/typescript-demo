import express from "express"
import randtoken from "rand-token"
import jwt from "jwt-simple";

const SECRET_TOKEN = "OUR_SECRET"
const refreshTokens: any = {};


export default class Auth {
    constructor(app: express.Application) {
        // login

        app.post("/login", (req: express.Request, res:express.Response, next: express.NextFunction) => {
            //checking if user exists ...
            const token = this.generateToken(req.body.username);
            const refreshToken = randtoken.uid(256);
            refreshTokens[refreshToken] = req.body.username;
            res.json({jwt: "JWT " + token, refreshToken: refreshToken});
        })

        //get new token
        app.post("/token", (req, res) => {
            try {
                const username = req.body.username;
                const refreshToken = req.body.refreshToken;

                if (
                    Object.keys(refreshTokens).includes(refreshToken) &&
                    refreshTokens[refreshToken] === username
                ) {
                    const token = this.generateToken(username);
                    res.json({token: "JWT " + token});
                } else {
                    res.status(401).end();
                }

            } catch (e) {
                console.error(e);
                res.status(401).end();
            }
        })

        app.get("/test_jwt", Auth.authenticate, (req, res) => {
            res.json({"type": "success"});
        })


        //test route
    }

    //middleware
    static authenticate(req: express.Request, res: express.Response, next: express.NextFunction) {
        try {
            const token = req.headers.authorization;
            const payload = jwt.decode(token ? token.split(" ")[1] : "", SECRET_TOKEN);

            if (payload) {
                if (payload.exp < new Date()) {
                    return res.status(401).end();
                }
            } else {
                return res.status(401).end();
            }
            next();
        } catch (e) {
            res.status(401).end();
            console.error(e)
        }
    }

    // token creation
    generateToken(name: string) {
        const user = {
            username: name,
            role: "Admin",
            iat: Number(new Date()),
            exp: Number(new Date()) + 1000 * 60 * 15
        }
        return jwt.encode(user, SECRET_TOKEN);
    }
}