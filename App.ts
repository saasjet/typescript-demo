import express from 'express';
import {MongoService} from "./services/mongo.service"
import {ConfigInterface} from "./interfaces/ConfigInterface";
import Auth from "./services/jwt.service";
// @ts-ignore

import cors from "cors";
import path from "path";
require("dotenv").config();

export class App {

    app: express.Application;
    config: ConfigInterface

    constructor() {

        this.app = express();
        this.config = require("./config.json");


        // const MainController = require("./controllers/main.controller");
        // const MongoService = require("./services/mongo.service");
        // const UserController = require("./controllers/user.controller");


        const es6Renderer = require("express-es6-template-engine");
        this.app.engine("html", es6Renderer);
        this.app.set("views", path.join(__dirname, "..", "client/public"));
        this.app.use(express.static(path.join(__dirname, "..", "client/public")));
        this.app.use(express.json());
        this.app.use(cors());
    }

    async init(){
        await new MongoService(this.config.mongo).init();
        new Auth(this.app);
    }

    getExpressApp(){
        return this.app;
    }
}