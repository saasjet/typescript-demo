export interface ConfigInterface {
    mongo: MongoInterface
}

export interface MongoInterface{
    url: string,
    db: string,
    collections: any
}